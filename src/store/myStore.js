export default {
  from: {
    road: '52 rue Jacques Babinet',
    zipCode: '31000',
    city: 'Toulouse',
    complement: 'Makina Corpus',
    country: 'Occitanie'
  },
  to: {
    road: 'place du Capitole',
    zipCode: '31000',
    city: 'Toulouse',
    state: 'France'
  },
  selectedAddress: '',
  results: [],
  overpassData: [],
  lat: 51.505,
  lon: -0.09,
  currentDisplayName: 'London'

}
